# This should be a test or example startup script
# The cellMods path needs to be specified, using  $(PWD), relative or absolute path
#epicsEnvSet(EPICS_DRIVER_PATH, "$(PWD)/cellMods:$(EPICS_DRIVER_PATH)")
require choppdu

iocshLoad("$(choppdu_DIR)choppdu.iocsh")

# always leave a blank line at EOF
